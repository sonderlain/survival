package survival.core.misc;

public class Config {
    public static int SCREEN_WIDTH = 1366;
    public static int SCREEN_HEIGHT = 760;

    public static int STEP_SIZE = 2;

    public static int FPS = 60;


    public static final int MONSTERS_SPAWN_OFFSET_LENGTH = 50;

}
