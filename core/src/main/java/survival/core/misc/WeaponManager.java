package survival.core.misc;

import survival.core.world.creatures.Weapon;

import java.util.ArrayList;

/**
 * Created by Akson on 08.05.2015.
 */
public class WeaponManager {
    public ArrayList<Weapon> weapons = new ArrayList<Weapon>();
    //Machinegun
    public int machinegunAmmo = 20;
    public int machinegunClipSize = 20;
    public int machinegunBulletSpeed = 30;
    public int machinegunBulletsRate = 4;
    public int machinegunPower = 25;
    public double machinegunDirectionDispersion = Math.PI / 40;
    public double machinegunDeathDistance = 0;
    //Pistol
    public int pistolAmmo = -1; //infinite
    public int pistolBulletSpeed = 15;
    public int pistolBulletsRate = 20;
    public int pistolPower = 30;
    public double pistolDeathDistance = 0;
    //shotgun
    public int shotgunAmmo = 10;
    public int shotgunClipSize = 15;
    public int shotgunBulletsAmount = 5;
    public double shotgunSpeedDispersion = 0.1;
    public double shotgunAperture = Math.PI / 4;
    public int shotgunBulletsRate = 75;
    public int shotgunBulletsSpeed = 10;
    public int shotgunPower = 50;
    public double shotgunDeathDistance = 0;

    public boolean isShooting = false;
    //flamethrower
    public int flamethrowerClipSize = 25;
    public int flamethrowerAmmo = 10;
    public int flamethrowerBulletSpeed = 15;
    public int flamethrowerBulletsRate = 3;
    public double flamethrowerPower = 0.01;
    public int flamethrowerAmplitude = 2;
    public int flamethrowerBulAmount = 15;
    public double flamethrowerArea = 15;
    public double flamethrowerAperture = Math.PI / 15;
    public double flamethrowerDeathDistance = 300;
    public double flamethrowerDeathDistanceDispersion = 30;
    public double flamethrowerHitRate = 2;
    //rocket
    public int rocketClipSize = 5;
    public int rocketAmmo = 0;
    public int rocketBulletSpeed = 5;
    public int rocketBulletsRate = 100;
    public int rocketPower = 1;
    public double rocketDeathDistance;
    public double rocketHitRate = 2;
    public double rocketExplosionPower = 100;
    public double rocketExplosionRadius = 30;
    public double rocketExplosionFlameDistance = 200;

    public Weapon getWeapon(int type){
        for(Weapon w : weapons){
            if(w.getType() == type){
                return w;
            }
        }
        return null;
    }

    public void addNewWeapon(int type) {
        if (getWeapon(type) == null) {
            weapons.add(new Weapon(type));
        }
    }

    public void addAmmo() {
        shotgunAmmo += shotgunClipSize;
        machinegunAmmo += machinegunClipSize;
        flamethrowerAmmo += flamethrowerClipSize;
        rocketAmmo += rocketClipSize;
    }
}
