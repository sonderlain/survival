package survival.core.misc;

import survival.core.world.creatures.Box;

public class Textures {

    public static final int ZOMBIES = 0;
    public static final int BLOOD = 1;
    public static final int FLAME = 2;

    private static String[] zombies = new String[] {"zombie1.png", "zombie2.png", "zombie3.png", "zombie4.png"};
    private static String[] blood = new String[] {"blood1.png", "blood2.png", "blood3.png", "blood4.png"};
    private static String[] flame = new String[] {"flame1.png", "flame2.png", "flame3.png"};
    private static String[] weapons = new String[] {"pistol.png", "shotgun.png", "machinegun.png", "flamethrower.png", "rocketlauncher.png"};

    public static String getTexture(int type) {
        String[] names = null;
        switch (type) {
            case ZOMBIES:
                names = zombies;
                break;
            case BLOOD:
                names = blood;
                break;
            case FLAME:
                names = flame;
                break;
            default:
                return "images/empty.png";
        }


        return "images/" + names[(int) (Math.random() * names.length)];
    }

    public static String getWeaponTexture(int type) {
        return "images/" + weapons[type];
    }

    public static String getBoxTexture(int boxType) {
        String name = "images/";
        switch (boxType) {
            case Box.MEDICAL_KIT:
                return name + "medkit.png";
            case Box.AMMO:
                return name + "weapons.png";
            default:
                return name + "empty.png";
        }

    }
}
