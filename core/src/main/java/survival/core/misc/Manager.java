package survival.core.misc;

import playn.core.GroupLayer;
import playn.core.Sound;
import survival.core.listeners.KeyboardState;
import survival.core.world.creatures.Box;
import survival.core.world.creatures.Hero;
import survival.core.world.creatures.Monster;
import survival.core.world.creatures.Thing;

import java.util.ArrayList;

public class Manager {
    private static Manager INSTANCE;

    public float mouseX;
    public float mouseY;
    public boolean mouseDown;
    public KeyboardState keyboardState;
    public Hero hero;

    public long frames;
    public long framesSinceShot;
    public ArrayList<Monster> monsters;
    public ArrayList<Box> boxes = new ArrayList<Box>();
    public GroupLayer backgroundLayer;
    public ArrayList<Sound> sounds = new ArrayList<Sound>();


    public static Manager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new Manager();
        return INSTANCE;
    }

    public void drawBloodSplash(float x, float y) {
        Thing blood = new Thing(Textures.getTexture(Textures.BLOOD), x, y, backgroundLayer);
        blood.rotate(hero.getRotation());
    }
}