package survival.core.misc;

import playn.core.Key;

public class Settings {
    public static class Keys {
        public static Key MOVE_RIGHT = Key.D;
        public static Key MOVE_LEFT = Key.A;
        public static Key MOVE_UP = Key.W;
        public static Key MOVE_DOWN = Key.S;
    }
}
