package survival.core.ui;

import playn.core.*;
import survival.core.misc.Config;
import survival.core.misc.Manager;
import survival.core.misc.Textures;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;

public class UIManager {

    private static final float HEALTH_X = 30;
    private static final float HEALTH_Y = 700;
    private static final float HEALTH_FONT_SIZE = 36;
    private static UIManager INSTANCE;
    private HealthBar healthBar = new HealthBar();
    private Manager manager = Manager.getInstance();
    private Canvas textView;

    private static final int OFFSET = 25;
    private static final int WEAPON_WIDTH = 64;


    private TextFormat format;
    private TextFormat smFormat;

    public static UIManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new UIManager();
        return INSTANCE;
    }

    public void initialize() {
        CanvasImage healthImage = graphics().createImage(1366, 760);
        textView = healthImage.canvas();
        //textView.setFillColor(0xFFFFFF);
        Font font = graphics().createFont("Sans serif", Font.Style.PLAIN, HEALTH_FONT_SIZE);
        format = new TextFormat().withFont(font);
        smFormat = new TextFormat().withFont(graphics().createFont("Sans serif", Font.Style.PLAIN, HEALTH_FONT_SIZE / 2));
        graphics().rootLayer().add(graphics().createImageLayer(healthImage));

        GroupLayer ammoLayer = graphics().createGroupLayer();
        for (int i = 0; i < 5; i++) {
            ImageLayer imageLayer = graphics().createImageLayer(assets().getImage(Textures.getWeaponTexture(i)));
            ammoLayer.add(imageLayer);
            imageLayer.setTranslation(Config.SCREEN_WIDTH - WEAPON_WIDTH * 5 + WEAPON_WIDTH * i, 30);
        }
        graphics().rootLayer().add(ammoLayer);
    }

    public void update() {
        textView.clear();
        TextLayout healthText = graphics().layoutText(
                "\u2665" + manager.hero.getHealth() + "", format);
        TextLayout pointsText = graphics().layoutText(
                "$" + manager.hero.getScores() + "", format);
        textView.fillText(healthText, HEALTH_X, HEALTH_Y);
        textView.fillText(pointsText, HEALTH_X + 125, HEALTH_Y);

        int ammo[] = new int[] {manager.hero.weaponManager.pistolAmmo, manager.hero.weaponManager.shotgunAmmo, manager.hero.weaponManager.machinegunAmmo, manager.hero.weaponManager.flamethrowerAmmo, manager.hero.weaponManager.rocketAmmo};

        for (int i = 0; i < 5; i++) {
            TextLayout t = graphics().layoutText(
                    ammo[i] == -1 ? "\u221e" : ammo[i] + "", smFormat);
            textView.fillText(t, Config.SCREEN_WIDTH - WEAPON_WIDTH * 5 + WEAPON_WIDTH * i + WEAPON_WIDTH / 2, 70);
        }
    }
}
