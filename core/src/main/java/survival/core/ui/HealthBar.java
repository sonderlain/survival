package survival.core.ui;

import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Surface;
import playn.core.SurfaceLayer;

import static playn.core.PlayN.graphics;

public class HealthBar {

    private final SurfaceLayer surfaceLayer;
    private final CanvasImage bar;

    private final static int MAX_HEALTH = 100;
    private Surface surface;
    private Canvas canvas;

    private int width = 200;
    private int height = 30;

    private int x = 25;
    private int y = 500;

    public HealthBar() {
        surfaceLayer = graphics().createSurfaceLayer(width, height);
        graphics().rootLayer().add(surfaceLayer);

        // create a solid background
        bar = graphics().createImage(width, height);

        canvas = bar.canvas();
        canvas.setFillColor(0xff87ceeb);
        setHealth(MAX_HEALTH);
    }

    public void setHealth(int health) {
        //canvas.fillRect(0, 0, width * health / MAX_HEALTH, height);

        /*surface = surfaceLayer.surface();
        surface.clear();
        surface.drawImage(bar, x, y);*/
    }
}
