package survival.core.world.creatures;

import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.Key;
import survival.core.listeners.KeyboardState;
import survival.core.misc.Config;
import survival.core.misc.Manager;
import survival.core.misc.Settings.Keys;
import survival.core.misc.Utils;
import survival.core.misc.WeaponManager;
import survival.core.ui.UIManager;

import java.util.ArrayList;
import java.util.ListIterator;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static survival.core.world.creatures.Weapon.createBullet;

/**
 * Главный персонаж
 */
public class Hero extends Creature {

    public ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    public ArrayList<Bullet> explosionBullets = new ArrayList<Bullet>();
    public Weapon currentWeapon = new Weapon(Weapon.PISTOL);
    public WeaponManager weaponManager = new WeaponManager();
    private UIManager uiManager = UIManager.getInstance();

    private int scores = 0;
    public boolean bulletsReady = true;


    public Hero() {
        //вызываем метод класса Creature. Он доступен, т.к. Hero наследуется от Creature
        super("images/hero.png", Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2);
        weaponManager.weapons.add(new Weapon(Weapon.PISTOL));
        weaponManager.weapons.add(new Weapon(Weapon.SHOTGUN));
        weaponManager.weapons.add(new Weapon(Weapon.MACHINEGUN));
        weaponManager.weapons.add(new Weapon(Weapon.FLAMETHROWER));
        weaponManager.weapons.add(new Weapon(Weapon.ROCKET));
    }

    /**
     * Обновляем позицию персонажа
     */
    public boolean shifted = false;

    public void setPosition(float x, float y) {
        //общий метод класса Creature, кем и является персонаж
        super.setPosition(x, y);
        //а это дополнительные действия, только для персонажа: поворот в сторону курсора
        //координаты курсора в данный момент хранятся в Manager.getInstance() и обновляются в методе setListeners()
        //класса Survival
        rotate(Manager.getInstance().mouseX, Manager.getInstance().mouseY);
    }


    /**
     * Управление с клавиатуры
     */
    public void updatePosition(KeyboardState keyboardState) {
        double diagonalStep = Config.STEP_SIZE / Math.sqrt(2);
        int N = Utils.booltoint(keyboardState.isKeyDown(Keys.MOVE_LEFT))
                + Utils.booltoint(keyboardState.isKeyDown(Keys.MOVE_RIGHT))
                + Utils.booltoint(keyboardState.isKeyDown(Keys.MOVE_UP))
                + survival.core.misc.Utils.booltoint(keyboardState.isKeyDown(Keys.MOVE_DOWN));
        if (N < 2) {
            if (keyboardState.isKeyDown(Keys.MOVE_LEFT))
                positionX -= Config.STEP_SIZE;
            if (keyboardState.isKeyDown(Keys.MOVE_RIGHT))
                positionX += Config.STEP_SIZE;
            if (keyboardState.isKeyDown(Keys.MOVE_UP))
                positionY -= Config.STEP_SIZE;
            if (keyboardState.isKeyDown(Keys.MOVE_DOWN))
                positionY += Config.STEP_SIZE;
        } else {
            if (keyboardState.isKeyDown(Keys.MOVE_LEFT))
                positionX -= diagonalStep;
            if (keyboardState.isKeyDown(Keys.MOVE_RIGHT))
                positionX += diagonalStep;
            if (keyboardState.isKeyDown(Keys.MOVE_UP))
                positionY -= diagonalStep;
            if (keyboardState.isKeyDown(Keys.MOVE_DOWN))
                positionY += diagonalStep;
        }

        setPosition();
    }

    public void update() {
        if (isAlive()) {
            super.update();
            updatePosition(manager.keyboardState);
                updateShooting(manager.mouseDown);
            updateWeapons();
        }
    }

    private void updateWeapons() {
        for(int i = Key.K1.ordinal();i <= Key.K9.ordinal();i++){
            if (manager.keyboardState.keys[i]){
                Weapon w = weaponManager.getWeapon(i - Key.K1.ordinal());
                if (w != null) {
                    currentWeapon = w;
                }
                break;
            }
        }
    }

    private void updateShooting(boolean mouseDown) {
        manager.framesSinceShot++;
        if (mouseDown) {
            createBullets();
        }
        weaponManager.isShooting = mouseDown;

        //updating bullets
        updateBullets(bullets);
        updateBullets(explosionBullets);

    }

    private void updateBullets(ArrayList<Bullet> bullets) {
        ListIterator<Bullet> it = bullets.listIterator();
        while (it.hasNext()) {
            Bullet bullet = it.next();
            bullet.move();
            if (bullet.positionX > Config.SCREEN_WIDTH + 50 || bullet.positionX < -50 || bullet.positionY > Config.SCREEN_HEIGHT + 50 || bullet.positionY < -50 || (bullet.distance > bullet.deathDistance && bullet.deathDistance != 0)) {

                bullet.remove();
                it.remove();
            }
        }
    }

    @Override
    protected void die() {
        super.die();
        System.out.println("The hero has died :(");
    }

    private void createBullets() {

        switch (currentWeapon.getType()) {
            case Weapon.PISTOL:
                if (manager.frames % weaponManager.pistolBulletsRate == 0) {
                    bullets.add(createBullet(currentWeapon.getType(), weaponManager, manager, positionX, positionY, this, 1));
                    //Sounds.play(Sounds.MACHINEGUN);
                }
                break;
            case Weapon.SHOTGUN:
                if (weaponManager.shotgunAmmo > 0 && (manager.frames % weaponManager.shotgunBulletsRate == 0 || !weaponManager.isShooting && manager.framesSinceShot > weaponManager.shotgunBulletsRate)) {
                    for (int i = 0; i < weaponManager.shotgunBulletsAmount; i++) {
                        bullets.add(createBullet(currentWeapon.getType(), weaponManager, manager, positionX, positionY, this, i));
                    }
                    manager.framesSinceShot = 0;
                    weaponManager.shotgunAmmo--;
                    //Sounds.play(Sounds.SHOTGUN);
                }
                break;
            case Weapon.MACHINEGUN:
                if (weaponManager.machinegunAmmo > 0 && (manager.frames % weaponManager.machinegunBulletsRate == 0)) {
                    bullets.add(createBullet(currentWeapon.getType(), weaponManager, manager ,positionX, positionY, this, 1));
                    //Sounds.play(Sounds.MACHINEGUN);
                    weaponManager.machinegunAmmo--;
                }

                break;
            case Weapon.FLAMETHROWER:
                if (weaponManager.flamethrowerAmmo > 0 && (manager.frames % weaponManager.flamethrowerBulletsRate == 0)){
                    for(int i = 1;i <= weaponManager.flamethrowerBulAmount;i++){
                        bullets.add(createBullet(currentWeapon.getType(), weaponManager, manager, positionX, positionY, this, i));
                    }
                    weaponManager.flamethrowerAmmo--;
                }
                break;
            case Weapon.ROCKET:
                if (weaponManager.rocketAmmo > 0 && (manager.frames % weaponManager.rocketBulletsRate == 0  || !weaponManager.isShooting && manager.framesSinceShot > weaponManager.rocketBulletsRate)) {
                    bullets.add(createBullet(currentWeapon.getType(), weaponManager, manager, positionX, positionY, this, 1));

                    manager.framesSinceShot = 0;
                    weaponManager.rocketAmmo--;
                }

        }
    }

    @Override
    public void hit(double damage) {
        super.hit(damage);
        uiManager.update();
    }

    private void weaponLayer(String imagePath) {
        Image image = assets().getImage(imagePath);
        ImageLayer weaponlayer = graphics().createImageLayer(image);
        grouplayer.add(weaponlayer);
    }

    public void addScore(int score) {
        scores += score;
        //System.out.println("scores: " + scores);
    }

    public int getScores() {
        return scores;
    }

    public boolean take(Box box) {
        switch (box.type) {
            case Box.MEDICAL_KIT:
                if (health == 100)
                    return false;
                health = 100;
                break;
            case Box.AMMO:
                weaponManager.addAmmo();
        }
        return true;
    }
}