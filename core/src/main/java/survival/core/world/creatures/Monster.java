package survival.core.world.creatures;

import survival.core.misc.Config;
import survival.core.misc.Textures;

public class Monster extends Creature {

    public static final int ZOMBIE = 0;

    private float speed = (float) (0.75 + Math.random() * 0.25);
    private float hitRate = 0.5f; // in seconds

    private int hitTime = (int) hitRate * 1000;


    public boolean isHitting = false;
    public int score = 10;

    private int type = ZOMBIE;

    public Monster(float x, float y, int type, float speed, float hitRate, int damage, int score) {
        this(x, y, type,score);
        this.speed = speed;
        this.hitRate = hitRate;
        this.damage = damage;
    }

    public Monster(float x, float y, int type, int score) {
        super(Textures.getTexture(type), x, y);
        this.type = type;
        this.score = score;
    }

    public void updatePosition(float heroPositionX, float heroPositionY) {
        rotate(heroPositionX, heroPositionY);

        double angle = Math.atan2(heroPositionY - positionY, heroPositionX - positionX);
        setPosition(positionX + (float) (speed * Math.cos(angle)), positionY + (float) (speed * Math.sin(angle)));
    }

    public boolean readyForHit() {
        if (hitTime >= hitRate * 1000 || !isHitting) {
            hitTime = 0;
            return true;
        } else {
            hitTime += 1000 / Config.FPS;
            return false;
        }
    }

    @Override
    protected void die() {
        super.die();
        manager.drawBloodSplash(positionX, positionY);
    }

}
