package survival.core.world.creatures;

import survival.core.misc.Manager;
import survival.core.misc.WeaponManager;

import java.util.Random;

/**
 * Created by Akson on 16.04.2015.
 */
public class Weapon {
    private int type;
    public final static int WEAPONS_COUNT = 6;
    public final static int PISTOL = 0;
    public final static int SHOTGUN = 1;
    public final static int MACHINEGUN = 2;
    public final static int FLAMETHROWER = 3;
    public final static int MINE = 5;
    public final static int ROCKET = 4;



    public Weapon(int newType){
        type = newType;
    }

    public int getType() {
        return type;
    }

    public static Bullet createBullet(int type, WeaponManager weaponManager, Manager manager, float positionX, float positionY, Hero hero, int i){
        double direction;
        switch (type) {
            case Weapon.PISTOL:
                return new Bullet(positionX, positionY, manager.mouseX, manager.mouseY, weaponManager.pistolBulletSpeed, weaponManager.pistolPower, Bullet.METAL, hero, weaponManager.pistolDeathDistance);
            case Weapon.SHOTGUN:
                direction = Math.atan2(manager.mouseY - positionY, manager.mouseX - positionX) +
                        (Math.floor((double) weaponManager.shotgunBulletsAmount / 2) - i) * weaponManager.shotgunAperture / weaponManager.shotgunBulletsAmount +
                        weaponManager.shotgunAperture / weaponManager.shotgunBulletsAmount * Math.random();
                return new Bullet(positionX, positionY, direction, weaponManager.shotgunBulletsSpeed * (1 + weaponManager.shotgunSpeedDispersion * (1 - 2 * Math.random())), weaponManager.shotgunPower, Bullet.METAL, hero, weaponManager.shotgunDeathDistance);
                    //Sounds.play(Sounds.SHOTGUN);
            case Weapon.MACHINEGUN:
                direction = Math.atan2(manager.mouseY - positionY, manager.mouseX - positionX) + weaponManager.machinegunDirectionDispersion * (1 - 2 * Math.random());
                return new Bullet(positionX, positionY, direction, weaponManager.machinegunBulletSpeed, weaponManager.machinegunPower, Bullet.METAL, hero, weaponManager.machinegunDeathDistance);
                    //Sounds.play(Sounds.MACHINEGUN);
            case Weapon.FLAMETHROWER:
                direction = Math.atan2(manager.mouseY - positionY, manager.mouseX - positionX) + weaponManager.flamethrowerAperture * (1 - 2 * Math.random());
                double phi = 2 * Math.PI * Math.random();
                double r = weaponManager.flamethrowerArea * Math.sqrt(Math.random());
                float length = (float) Math.sqrt(Math.pow(manager.mouseX - positionX, 2) + Math.pow(manager.mouseY - positionY, 2));
                return new Bullet(positionX + (float) weaponManager.flamethrowerArea * (manager.mouseX - positionX) / length + (float) r * (float) Math.cos(phi),
                        positionY + (float) weaponManager.flamethrowerArea * (manager.mouseY - positionY) / length + (float) r * (float) Math.sin(phi), direction,
                        weaponManager.flamethrowerBulletSpeed, weaponManager.flamethrowerPower / 1.0, Bullet.FLAME, hero,
                        weaponManager.flamethrowerDeathDistance + weaponManager.flamethrowerDeathDistanceDispersion * (float) (new Random()).nextGaussian());
            case Weapon.MINE:
                return new Bullet(positionX, positionY, manager.mouseX, manager.mouseY, weaponManager.pistolBulletSpeed, weaponManager.pistolPower, Bullet.METAL, hero, weaponManager.pistolDeathDistance);
            case Weapon.ROCKET:
                weaponManager.rocketDeathDistance = Math.sqrt((manager.mouseX - positionX) * (manager.mouseX - positionX)
                        + (manager.mouseY - positionY) * (manager.mouseY - positionY));
                return new Bullet(positionX, positionY, manager.mouseX, manager.mouseY, weaponManager.rocketBulletSpeed, weaponManager.rocketPower, Bullet.ROCKET, hero, weaponManager.rocketDeathDistance);
        }
        return null;
    }
}
