package survival.core.world.creatures;

/**
 * Класс, описывающий что-либо, что будет перемещаться по экрану (персонаж, монстры)
 */
public class Creature extends Thing {

    protected int health = 100;
    protected int damage = 10;

    private boolean alive = true;

    public Creature(String imagePath, float x, float y) {
        super(imagePath, x, y);
    }


    protected void die() {
        alive = false;
        active = false;
        remove();
    }

    public void hit(double damage) {
        health -= damage;
        if (health <= 0 && alive) {
            die();
        }
        //System.out.println(health);
    }

    public int getDamage() {
        return damage;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getHealth() {
        return health;
    }
}
