package survival.core.world.creatures;

import survival.core.misc.Textures;

import java.util.Random;

public class Bullet extends Creature {

    private final double angle;
    public Hero owner;
    public static final int METAL = 0;
    public static final int FLAME = 1;
    public static final int ROCKET = 2;
    public double speed;
    public double power;
    public double distance;
    public double deathDistance;
    public Monster target;
    public boolean isReady = true;
    public long hitTime = 0;
    public double speed0;
    public float targetX;
    public float targetY;
    public int type = METAL;

    public Bullet(float x, float y, float targetX, float targetY, int speed, int power, int type, Hero owner, double deathDistance) {
        super(type == FLAME ? Textures.getTexture(Textures.FLAME) : type == ROCKET ? "images/rocket.png" : "images/bullet.png", x, y);
        this.power = power;
        this.deathDistance = deathDistance;
        angle = Math.atan2(targetY - y, targetX - x);
        this.targetX = targetX;
        this.targetY = targetY;
        this.speed = speed;
        this.speed0 = speed;
        this.type = type;
        this.owner = owner;
        rotate((float) angle);
    }

    public Bullet(float x, float y, double angle, double speed, double power, int type, Hero owner, double deathDistance) {
        super(type == FLAME ? Textures.getTexture(Textures.FLAME) : type == ROCKET ? "images/rocket.png" : "images/bullet.png", x, y);
        this.targetX = x;
        this.targetY = y;
        this.speed = speed;
        this.angle = angle;
        this.deathDistance = deathDistance;
        this.speed0 = speed;
        System.out.println(angle);
        this.power = power;
        this.type = type;
        this.owner = owner;
        rotate((float) angle);
    }

    public void move() {
        switch (type) {
            case METAL:
            case ROCKET:
                distance += speed;
                setPosition(positionX + (float) (speed * Math.cos(angle)), positionY + (float) (speed * Math.sin(angle)));
                break;
            case FLAME:
                speed = speed0 * (1 - distance / deathDistance);
                float stepX = (float) (speed * Math.cos(angle)) + owner.weaponManager.flamethrowerAmplitude * (float) (new Random()).nextGaussian();
                float stepY = (float) (speed * Math.sin(angle)) + owner.weaponManager.flamethrowerAmplitude * (float) (new Random()).nextGaussian();
                setPosition(positionX + stepX, positionY + stepY);
                distance += Math.sqrt(stepX * stepX + stepY * stepY);
                break;
        }
    }

    @Override
    public void remove() {
        super.remove();

        if (type == ROCKET) {
            for (int i = 0; i < owner.weaponManager.rocketExplosionPower; i++) {
                owner.explosionBullets.add(new Bullet(positionX + (float) (new Random()).nextGaussian() * (float) owner.weaponManager.rocketExplosionRadius,
                        positionY + (float) (new Random()).nextGaussian() * (float) owner.weaponManager.rocketExplosionRadius, 0, 0, owner.weaponManager.rocketPower, Bullet.FLAME, owner, owner.weaponManager.rocketExplosionFlameDistance));
            }
        }
    }

    @Override
    public void update() {
        super.update();
    }
}
