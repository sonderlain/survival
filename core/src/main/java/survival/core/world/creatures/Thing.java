package survival.core.world.creatures;

import playn.core.*;
import playn.core.util.Callback;
import survival.core.misc.Manager;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;

public class Thing {

    //Координаты
    protected float positionX;
    protected float positionY;

    protected float radius;

    //Слой изображения, привязанного к существу
    protected GroupLayer grouplayer;
    protected ImageLayer layer;

    protected Manager manager;
    protected boolean active = true;
    private GroupLayer parent;

    public Thing(String imagePath, float x, float y) {
        this(imagePath, x, y, graphics().rootLayer());
    }

    public Thing(String imagePath, float x, float y, boolean background) {
        this(imagePath, x, y, graphics().rootLayer());
    }

    public Thing(String imagePath, float x, float y, GroupLayer parent) {
        this.parent = parent;
        //Создаем картинку
        Image image = assets().getImage(imagePath);
        //Создаем слой
        grouplayer = graphics().createGroupLayer();
        //Привязываем к основному слою игры
        layer = graphics().createImageLayer(image);
        parent.add(grouplayer);
        grouplayer.add(layer);
        setPosition(x, y);
        manager = Manager.getInstance();
        image.addCallback(new Callback<Image>() {
            @Override
            public void onSuccess(Image image) {
                layer.setOrigin(image.width() / 2, image.height() / 2);

                radius = Math.max(image.width() / 2, image.height() / 2);
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }



    public float getPositionX() {
        return positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public float getRadius() {
        return radius;
    }

    /**
     * Устанавливает координаты существа
     */
    public void setPosition(float x, float y) {
        positionX = x;
        positionY = y;

        grouplayer.setTranslation(x, y);
    }

    public float getRotation() {
        return layer.rotation();
    }

    /**
     * Обновляет координаты существа
     */
    public void setPosition() {
        setPosition(positionX, positionY);
    }

    public void update() {

    }

    public boolean collideWith(Thing thing) {
        if (!thing.isActive())
            return false;
        float tX = thing.getPositionX();
        float tY = thing.getPositionY();
        double dist = Math.sqrt((tX - positionX) * (tX - positionX) + (tY - positionY) * (tY - positionY));
        return dist < thing.getRadius() + getRadius();
    }

    public void remove() {
        active = false;
        parent.remove(grouplayer);
    }

    public boolean isActive() {
        return active;
    }

    /**
     * Поворачивает в сторону точки (x,y)
     */
    public void rotate(float x, float y) {
        float angle = (float) Math.atan2(y - positionY, x - positionX);
        rotate(angle);
    }

    public void rotate(float angle) {
        layer.setRotation(angle);
    }
}
