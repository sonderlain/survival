package survival.core.world.creatures;

import survival.core.misc.Textures;
import survival.core.world.Survival;

public class Box extends Thing {
    public int type;

    private static final int WIDTH = 23;
    private static final int HEIGHT = 23;

    public static final int MEDICAL_KIT = 0;
    public static final int AMMO = 1;

    public Box(String imagePath, float x, float y) {
        super(imagePath, x, y, Survival.BACKGROUND_LAYER);

        layer.setWidth(WIDTH);
        layer.setHeight(HEIGHT);
    }

    public Box(int type, float x, float y) {
        this(Textures.getBoxTexture(type), x, y);
        this.type = type;
    }

    public static int getRandomType() {
        return Math.random() * 2 < 1 ? 0 : 1;
    }

    public int getType() {
        return type;
    }
}
