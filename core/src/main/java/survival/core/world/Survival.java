package survival.core.world;

import playn.core.*;
import survival.core.listeners.KeyboardState;
import survival.core.misc.Config;
import survival.core.misc.Manager;
import survival.core.ui.UIManager;
import survival.core.world.creatures.*;

import java.util.ArrayList;
import java.util.Iterator;

import static playn.core.PlayN.*;

public class Survival extends Game.Default {

    private Hero hero;
    private ArrayList<Monster> monsters = new ArrayList<Monster>();
    private Manager manager;

    private int monstersSpawnTime;

    private float monstersSpawnInterval = 1f; // in seconds
    private float minMonstersSpawnInterval = 0.33f; // in seconds
    private int monstersSpawnIntervalIncrease = 500; // in scores

    private float boxesSpawnInterval = 100; // in scores


    public static GroupLayer BACKGROUND_LAYER;

    private boolean boxReady = true;

    public Survival() {
        super(1000 / Config.FPS);
    }

    @Override
    public void init() {
        // бэкграунд
        Image bgImage = assets().getImage("images/bg.png");
        GroupLayer bgLayer = graphics().createGroupLayer();
        bgLayer.add(graphics().createImageLayer(bgImage));
        graphics().rootLayer().add(bgLayer);

        //класс для хранения переменных вроде позиции курсора
        manager = Manager.getInstance();
        manager.backgroundLayer = bgLayer;
        BACKGROUND_LAYER = bgLayer;
        manager.monsters = monsters;
        //обработчик клавиатуры
        manager.keyboardState = KeyboardState.getState();
        keyboard().setListener(manager.keyboardState);
        //создаем персонажа
        hero = new Hero();
        manager.hero = hero;
        //листенер будет считывать изменение координаты мышки и обновлять эти координаты в Manager
        //а потом персонаж использует их для поворота
        mouse().setListener(new Mouse.Adapter() {
            @Override
            public void onMouseMove(Mouse.MotionEvent event) {
                Manager.getInstance().mouseX = event.x();
                Manager.getInstance().mouseY = event.y();
                hero.rotate(event.x(), event.y());
            }

            @Override
            public void onMouseDown(Mouse.ButtonEvent event) {
                if (event.button() == Mouse.BUTTON_LEFT) {
                    Manager.getInstance().mouseDown = true;
                }
            }

            @Override
            public void onMouseUp(Mouse.ButtonEvent event) {
                if (event.button() == Mouse.BUTTON_LEFT) {
                    Manager.getInstance().mouseDown = false;
                }
            }
        });

        UIManager.getInstance().initialize();
    }

    /**
     * Метод вызывается fps раз в секунду - мы используем его для обновления координат объектов
     */
    @Override
    public void update(int delta) {
        manager.frames++;
        //обновляем состояние нажатых клавиш
        manager.keyboardState = KeyboardState.getState();
        //обновляем позицию персонажа

        hero.update();
        manageMonsters();
        manageBoxes();
        checkCollisions();
        UIManager.getInstance().update();
        //updateSounds();

        if (hero.getScores() % monstersSpawnIntervalIncrease == 0 && monstersSpawnInterval > 0.5) {
            monstersSpawnInterval -= 0.05;
        }
    }


    private void manageBoxes() {
        if (manager.hero.getScores() % boxesSpawnInterval == 0 && boxReady) {
            boxReady = false;
            Box box = new Box(Box.getRandomType(), (float) Math.random() * Config.SCREEN_WIDTH, (float) Math.random() * Config.SCREEN_HEIGHT);
            manager.boxes.add(box);
        }

    }

    private void updateSounds() {
        for (Iterator<Sound> soundIterator = manager.sounds.iterator(); soundIterator.hasNext();) {
            Sound sound = soundIterator.next();
            if (!sound.isPlaying()) {
                sound.release();
                soundIterator.remove();
            }
        }
    }

    private void checkCollisions() {
        for (Iterator<Monster> monsterIterator = monsters.iterator(); monsterIterator.hasNext();) {
            Monster monster = monsterIterator.next();
            if (monster.collideWith(hero)){
                if (monster.readyForHit()) {
                    hero.hit(monster.getDamage());
                }
            }
            monster.isHitting = monster.collideWith(hero);

            iterateBullets(monster, monsterIterator, hero.bullets);
            iterateBullets(monster, monsterIterator, hero.explosionBullets);
        }


        for (Iterator<Box> boxIterator = manager.boxes.iterator(); boxIterator.hasNext();) {
            Box box = boxIterator.next();
            if (box.collideWith(hero)) {
                if (hero.take(box)) {
                    boxIterator.remove();
                    box.remove();
                }
            }
        }
    }

    private void iterateBullets(Monster monster, Iterator<Monster> mIterator, ArrayList<Bullet> bullets) {
        for (Iterator<Bullet> bulletIterator = bullets.iterator(); bulletIterator.hasNext(); ) {
            Bullet bullet = bulletIterator.next();
            if (bullet.collideWith(monster)) {
                if(bullet.type == Bullet.FLAME){
                    if ((manager.frames - bullet.hitTime) >= 60 / hero.weaponManager.flamethrowerHitRate){
                        bullet.isReady = true;
                    }
                    if(!monster.equals(bullet.target) || bullet.isReady){
                        bullet.target = monster;
                        bullet.isReady = false;
                        monster.hit(bullet.power);
                    }
                }
                else{
                    bulletIterator.remove();
                    bullet.remove();
                    monster.hit(bullet.power);
                }
                if (!monster.isAlive() && monsters.contains(monster)) {
                    boxReady = true;
                    bullet.owner.addScore(monster.score);
                    mIterator.remove();
                }
            }
        }
    }

    private void manageMonsters() {
        monstersSpawnTime += 1000 / Config.FPS;
        if (monstersSpawnTime > monstersSpawnInterval * 1000) {
            monstersSpawnTime = 0;
            monsters.add(createMonster());
        }

    }

    private Monster createMonster() {
        int side = (int) (Math.random() * 4);
        int x, y;
        x = y = -Config.MONSTERS_SPAWN_OFFSET_LENGTH;
        switch (side) {
            case 0:
                y = (int) (Math.random() * Config.SCREEN_HEIGHT);
                break;
            case 1:
                x = (int) (Math.random() * Config.SCREEN_WIDTH);
                break;
            case 2:
                x = Config.SCREEN_WIDTH + Config.MONSTERS_SPAWN_OFFSET_LENGTH;
                y = (int) (Math.random() * Config.SCREEN_HEIGHT);
                break;
            case 3:
                y = Config.SCREEN_HEIGHT + Config.MONSTERS_SPAWN_OFFSET_LENGTH;
                x = (int) (Math.random() * Config.SCREEN_WIDTH);
                break;
        }
        return new Monster(x, y, Monster.ZOMBIE, 10);
    }

    @Override
    public void paint(float alpha) {
        for (Monster monster : monsters) {
            if (monster != null && monster.isAlive()) {
                monster.update();
                monster.updatePosition(hero.getPositionX(), hero.getPositionY());
            }
        }
    }
}
