package survival.core.listeners;

import playn.core.Key;
import playn.core.Keyboard;

public class KeyboardState implements Keyboard.Listener {

    private static KeyboardState state = new KeyboardState();

    public static KeyboardState getState() {
        return state;
    }

    public boolean[] keys = new boolean[Key.values().length];

    public boolean isKeyDown(int keyCode) {
        assert (keyCode >= 0) && (keyCode < 256);
        return keys[keyCode];
    }

    public boolean isKeyDown(Key key) {
        return keys[key.ordinal()];
    }


    @Override
    public void onKeyDown(Keyboard.Event event) {
        keys[event.key().ordinal()] = true;
    }

    @Override
    public void onKeyTyped(Keyboard.TypedEvent event) {
    }

    @Override
    public void onKeyUp(Keyboard.Event event) {
        keys[event.key().ordinal()] = false;
    }
}
