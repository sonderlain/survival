package survival.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import survival.core.misc.Config;
import survival.core.world.Survival;

public class SurvivalJava {

    public static void main(String[] args) {
        JavaPlatform.Config config = new JavaPlatform.Config();
        // use config to customize the Java platform, if needed
        config.width = Config.SCREEN_WIDTH;
        config.height = Config.SCREEN_HEIGHT;
        JavaPlatform.register(config);
        PlayN.run(new Survival());
    }
}
