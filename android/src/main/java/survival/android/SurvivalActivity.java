package survival.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import survival.core.Survival;

public class SurvivalActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new Survival());
  }
}
