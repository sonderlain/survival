# Zombie Survival #
A top-down view zombie shooter. The player is placed in the middle of the map while enemies gradually enter and make their way towards the player. Shoot zombies until you die.

## How to play ##
Move across the field and shoot enemies using different weapons (Pistol, Machinegun, Shotgun, Flamethrower, Rocket launcher). Be careful because you have limited health and ammo. You can restore them by collecting boxes appearing on the ground.

## How to launch ##
Install git, Maven, JRE>=1.6

```
#!bash

git clone https://bitbucket.org/cyberdd3/survival
```

 
```
#!bash

mvn test -Pjava
```


## Credits ##
* **Ivan Suvorov** general game structure: hero, monsters, bullets
* **Ilya Kudrov** weapons
* **Darya Chernikova** boxes, user interface
* *PlayN Java Game Engine Authors and Contributors*